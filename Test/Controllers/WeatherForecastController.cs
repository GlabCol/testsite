﻿using Application.Interface;
using Domain;
using Infrastructure.Interface;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Test.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private readonly IGameService gameService;
        public WeatherForecastController(IGameService gameService)
        {
            this.gameService = gameService;
        }
        [HttpGet]
        public ActionResult<Game> Get([FromQuery]decimal minPrice, [FromQuery]decimal maxPrice)
        {
            var result = gameService.GetGames(minPrice, maxPrice);
            if (result == null) { return BadRequest(); }
            return Ok(result);
        }
    }
}
