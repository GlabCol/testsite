﻿namespace Domain
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    public class Game
    {
        public int Id { get; private set; }

        public string Title { get; private set; }

        public decimal Price { get; private set; }

        public Game(int id, string title, decimal price) 
        {
            Id = id;
            Title = title;
            Price = price;
        }
    }
}
