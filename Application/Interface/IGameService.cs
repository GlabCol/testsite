﻿using Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Interface
{
    public interface IGameService
    {
        List<Game> GetGames(decimal minPrice, decimal maxPrice);
    }
}
