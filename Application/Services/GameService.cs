﻿

namespace Application.Services
{
    using Application.Interface;
    using Domain;
    using Infrastructure.Interface;
    using System;
    using System.Collections.Generic;
    using System.Text;
    public class GameService : IGameService
    {
        private readonly IGameRepository gameRepository;

        public GameService(IGameRepository gameRepository) 
        {
            this.gameRepository = gameRepository;
        }

        public List<Game> GetGames(decimal minPrice, decimal maxPrice)
        {
            if (Validate(minPrice, maxPrice))
            {
                return gameRepository.GetGames(minPrice, maxPrice);
            }
            return null;
        }

        private bool Validate(decimal minPrice, decimal maxPrice)
        {
            return minPrice >= 0 && maxPrice >= minPrice;
        }
    }
}
