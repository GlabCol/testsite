﻿

namespace Infrastructure.Repository
{
    using System.Linq;
    using Domain;
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Infrastructure.Interface;

    public class GameRepository : IGameRepository
    {
        private List<Game> games = new List<Game>()
        {
            new Game(1, "Doka 2", 0),
            new Game(2, "CS:GO", 500),
            new Game(3, "Minecraft", 20000)
        };
        public List<Game> GetGames(decimal minPrice, decimal maxPrice)
        {
            return games.Where(g => g.Price >= minPrice && g.Price <= maxPrice).ToList();
        }
    }
}
