﻿

namespace Infrastructure.Interface
{
    using Domain;
    using System;
    using System.Collections.Generic;
    using System.Text;
    public interface IGameRepository
    {
        List<Game> GetGames(decimal minPrice, decimal maxPrice);
    }
}
